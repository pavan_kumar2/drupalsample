<?php
/**
 * @file
 * Implement configuarion form to connect slack account
 */
function drupal_to_slack_configuration_form() {
  $form['drupal_to_slack_incoming_webhook_url'] = array(
    '#type' => 'textfield',
    '#title' => 'Incoming webhook url',
    '#description' => 'Enter incoming webhook url from slack account',
    '#default_value' => variable_get('drupal_to_slack_incoming_webhook_url'),
  );
  $form['drupal_to_slack_channel_for_message'] = array(
    '#type' => 'textfield',
    '#title' => 'Slack channel',
    '#description' => 'Enter slack channel for notification',
    '#default_value' => variable_get('drupal_to_slack_channel_for_message'),
  );
  $node_type = node_type_get_types();
  foreach ($node_type as $node) {
    $type[$node->type] = $node->name;
  }
  $form['node_to_notify_on_slack'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Content type to notify on slack',
    '#options' => $type,
    '#default_value' => variable_get('node_to_notify_on_slack'),
  );
  return system_settings_form($form);
}
