<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/open_banking_portal_revised/templates/system/page--user--login.html.twig */
class __TwigTemplate_041d00e96366c8b7aa27329aa4bfe41dbdb3b4cacc96e805633dda9269ab7c63 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'header' => [$this, 'block_header'],
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 9, "block" => 31];
        $filters = ["escape" => 3, "t" => 17];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'block'],
                ['escape', 't'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"login-page\">
<div class=\"col-sm-6 col-md-5 left-side-image\">
 <img src=\"";
        // line 3
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/Login-Bigscreen.png\">
</div>

<div class=\"col-sm-6 col-md-7 right-side-form\">
<section class=\"open_banking_header\">
  <header";
        // line 8
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["navbar_attributes"] ?? null), "addClass", [0 => ($context["navbar_classes"] ?? null)], "method")), "html", null, true);
        echo " id=\"navbar\" role=\"banner\">
    ";
        // line 9
        if ( !$this->getAttribute(($context["navbar_attributes"] ?? null), "hasClass", [0 => ($context["container"] ?? null)], "method")) {
            // line 10
            echo "    <div class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
            echo "\">
      ";
        }
        // line 12
        echo "      <div class=\"navbar-header\">
        ";
        // line 13
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navigation", [])), "html", null, true);
        echo "
        ";
        // line 15
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])) {
            // line 16
            echo "          <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#navbar-collapse\">
            <span class=\"sr-only\">";
            // line 17
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Toggle navigation"));
            echo "</span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
          </button>
        ";
        }
        // line 23
        echo "      </div>
    </div>
  </header>
</section>
  <div class=\"login-form\">
    <h2>Login</h2>
    ";
        // line 30
        echo "    ";
        if ($this->getAttribute(($context["page"] ?? null), "header", [])) {
            // line 31
            echo "      ";
            $this->displayBlock('header', $context, $blocks);
            // line 36
            echo "    ";
        }
        // line 37
        echo "  ";
        // line 38
        echo "  ";
        $this->displayBlock('content', $context, $blocks);
        // line 42
        echo "</div>
</div>
</div>";
    }

    // line 31
    public function block_header($context, array $blocks = [])
    {
        // line 32
        echo "        <div class=\"col-sm-12\" role=\"heading\">
          ";
        // line 33
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
        echo "
        </div>
      ";
    }

    // line 38
    public function block_content($context, array $blocks = [])
    {
        // line 39
        echo "    <a id=\"main-content\"></a>
    ";
        // line 40
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "
  ";
    }

    public function getTemplateName()
    {
        return "themes/custom/open_banking_portal_revised/templates/system/page--user--login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 40,  147 => 39,  144 => 38,  137 => 33,  134 => 32,  131 => 31,  125 => 42,  122 => 38,  120 => 37,  117 => 36,  114 => 31,  111 => 30,  103 => 23,  94 => 17,  91 => 16,  88 => 15,  84 => 13,  81 => 12,  75 => 10,  73 => 9,  69 => 8,  61 => 3,  57 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"login-page\">
<div class=\"col-sm-6 col-md-5 left-side-image\">
 <img src=\"{{base_path ~ directory }}/images/Login-Bigscreen.png\">
</div>

<div class=\"col-sm-6 col-md-7 right-side-form\">
<section class=\"open_banking_header\">
  <header{{ navbar_attributes.addClass(navbar_classes) }} id=\"navbar\" role=\"banner\">
    {% if not navbar_attributes.hasClass(container) %}
    <div class=\"{{ container }}\">
      {% endif %}
      <div class=\"navbar-header\">
        {{ page.navigation }}
        {# .btn-navbar is used as the toggle for collapsed navbar content #}
        {% if page.navigation_collapsible %}
          <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#navbar-collapse\">
            <span class=\"sr-only\">{{ 'Toggle navigation'|t }}</span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
          </button>
        {% endif %}
      </div>
    </div>
  </header>
</section>
  <div class=\"login-form\">
    <h2>Login</h2>
    {# Header #}
    {% if page.header %}
      {% block header %}
        <div class=\"col-sm-12\" role=\"heading\">
          {{ page.header }}
        </div>
      {% endblock %}
    {% endif %}
  {# Content #}
  {% block content %}
    <a id=\"main-content\"></a>
    {{ page.content }}
  {% endblock %}
</div>
</div>
</div>", "themes/custom/open_banking_portal_revised/templates/system/page--user--login.html.twig", "/Applications/MAMP/htdocs/oldob/ob-developer-portal/web/themes/custom/open_banking_portal_revised/templates/system/page--user--login.html.twig");
    }
}
