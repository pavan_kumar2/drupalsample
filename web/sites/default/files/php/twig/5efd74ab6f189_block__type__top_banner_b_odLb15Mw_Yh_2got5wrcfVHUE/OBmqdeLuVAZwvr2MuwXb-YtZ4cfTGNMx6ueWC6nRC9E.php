<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/open_banking_portal_revised/templates/system/block/block__type__top_banner_block.html.twig */
class __TwigTemplate_469285466e74e9cf982db31fbd37568826cfcaeda88000686193f35e23ffba7d extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 7];
        $filters = ["escape" => 1];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "home-page-top-bar contact-us-page"], "method")), "html", null, true);
        echo " >
    ";
        // line 3
        echo "    ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_background", [])), "html", null, true);
        echo "
    <div class=\"top-banner-body\">
        ";
        // line 5
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_banner_title", [])), "html", null, true);
        echo "
        ";
        // line 6
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "body", [])), "html", null, true);
        echo "
        ";
        // line 7
        if ( !twig_test_empty($this->getAttribute(($context["content"] ?? null), "field_banner_links", []))) {
            // line 8
            echo "
            ";
            // line 9
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_banner_links", [])), "html", null, true);
            echo "

        ";
        }
        // line 12
        echo "        ";
        // line 13
        echo "    </div>

</div>

";
    }

    public function getTemplateName()
    {
        return "themes/custom/open_banking_portal_revised/templates/system/block/block__type__top_banner_block.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 13,  85 => 12,  79 => 9,  76 => 8,  74 => 7,  70 => 6,  66 => 5,  60 => 3,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<div {{ attributes.addClass('home-page-top-bar contact-us-page') }} >
    {#<div class=\"row banner-title\">#}
    {{content.field_background}}
    <div class=\"top-banner-body\">
        {{content.field_banner_title}}
        {{content.body}}
        {% if content.field_banner_links is not empty  %}

            {{content.field_banner_links}}

        {% endif %}
        {#</div>#}
    </div>

</div>

", "themes/custom/open_banking_portal_revised/templates/system/block/block__type__top_banner_block.html.twig", "/Applications/MAMP/htdocs/oldob/ob-developer-portal/web/themes/custom/open_banking_portal_revised/templates/system/block/block__type__top_banner_block.html.twig");
    }
}
