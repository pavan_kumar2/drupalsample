<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/test_restapi/templates/open-banking-my-apps.html.twig */
class __TwigTemplate_15b8d1b2e82d711ff2cb92038974f46fc72b6e92f730c35835713cf7c2241020 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 1, "if" => 13, "for" => 16];
        $filters = ["escape" => 10, "replace" => 24, "date" => 58, "merge" => 79, "join" => 121];
        $functions = ["url" => 10];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'for'],
                ['escape', 'replace', 'date', 'merge', 'join'],
                ['url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["apiProductsMap"] = [];
        // line 2
        $context["apiProducts"] = [];
        // line 3
        $context["never"] = "Never";
        // line 4
        echo "<div class=\"my-apps-details\">
    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12\">
                <div class=\"my-app-add-button\">
                    <div class=\"btn btn-default\">
                        <a class=\"use-ajax\" data-dialog-type=\"modal\" href=\"";
        // line 10
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("entity.developer_app.add_form_for_developer", ["user" => ($context["user_id"] ?? null)]), "html", null, true);
        echo "\"><label>+</label> Add an App</a>
                    </div>
                </div>               
                ";
        // line 13
        if ( !twig_test_empty(($context["my_apps"] ?? null))) {
            // line 14
            echo "                    <div class=\"my-app-list-details\">
                        <dl class=\"ckeditor-accordion\">
                            ";
            // line 16
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["my_apps"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["developerApp"]) {
                // line 17
                echo "                                <dt>
                                    ";
                // line 18
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["developerApp"], "displayName", [])), "html", null, true);
                echo "                                    
                                </dt>
                                <dd>
                                    <div class=\"panel with-nav-tabs panel-info\">
                                        <div class=\"panel-heading\">
                                            <ul class=\"nav nav-tabs\">
                                                <li class=\"active\"><a href=\"#my-app-keys-";
                // line 24
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_replace_filter($this->sandbox->ensureToStringAllowed($this->getAttribute($context["developerApp"], "name", [])), "_", "-"), "html", null, true);
                echo "\" data-toggle=\"tab\"><img src=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
                echo "/images/key.png\">Keys</a></li>
                                                <li><a href=\"#my-app-api-products-";
                // line 25
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_replace_filter($this->sandbox->ensureToStringAllowed($this->getAttribute($context["developerApp"], "name", [])), "_", "-"), "html", null, true);
                echo "\" data-toggle=\"tab\"><img src=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
                echo "/images/product.png\">Products</a></li>
                                                <li><a href=\"#my-app-details-";
                // line 26
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_replace_filter($this->sandbox->ensureToStringAllowed($this->getAttribute($context["developerApp"], "name", [])), "_", "-"), "html", null, true);
                echo "\" data-toggle=\"tab\"><img src=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
                echo "/images/details.png\">Details</a></li>
                                                <li><a href=\"";
                // line 27
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("test_restapi.my_app_analytics_controller_getMyAppName", ["app" => $this->getAttribute($context["developerApp"], "name", [])]), "html", null, true);
                echo "\"><img src=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
                echo "/images/analytics.png\">Analytics</a></li>
                                                <li><a class=\"use-ajax\" data-dialog-type=\"modal\" href=\"";
                // line 28
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("entity.developer_app.edit_form_for_developer", ["user" => ($context["user_id"] ?? null), "app" => $this->getAttribute($context["developerApp"], "name", [])]), "html", null, true);
                echo "\"><img src=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
                echo "/images/edit.png\">Edit</a></li>
                                                <li><a class=\"use-ajax\" data-dialog-type=\"modal\" href=\"";
                // line 29
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("entity.developer_app.delete_form_for_developer", ["user" => ($context["user_id"] ?? null), "app" => $this->getAttribute($context["developerApp"], "name", [])]), "html", null, true);
                echo "\"><img src=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
                echo "/images/delete.png\">Delete</a></li>
                                            </ul>
                                        </div>
                                        <div class=\"panel-body\">
                                            <div class=\"tab-content\">
                                                <div class=\"tab-pane fade in active my-app-keys\" id=\"my-app-keys-";
                // line 34
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_replace_filter($this->sandbox->ensureToStringAllowed($this->getAttribute($context["developerApp"], "name", [])), "_", "-"), "html", null, true);
                echo "\">
                                                    ";
                // line 35
                if ( !twig_test_empty($this->getAttribute($context["developerApp"], "Credentials", []))) {
                    // line 36
                    echo "                                                        ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["developerApp"], "Credentials", []));
                    foreach ($context['_seq'] as $context["_key"] => $context["credential"]) {
                        // line 37
                        echo "                                                            <div class=\"my-apps-title\"
                                                                 <span>";
                        // line 38
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["developerApp"], "displayName", [])), "html", null, true);
                        echo "'s Keys</span>
                                                            </div>
                                                            <div class=\"my-apps-description\">
                                                                <p>Below are keys you can use to access the API products associated with this application (Delta digitalapi). The actual keys need to be approved and approved for an API product to be capable of accessing any of the URIs defined in the API product.</p>
                                                            </div>
                                                            <div class=\"my-apps-keys\">
                                                                <table class=\"table table-striped\">
                                                                    <tbody>
                                                                        <tr>
                                                                            <th scope=\"row\">Consumer Key</th>
                                                                            <td>";
                        // line 48
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["credential"], "consumerKey", [])), "html", null, true);
                        echo "</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope=\"row\">Consumer Secret</th>
                                                                            <td>";
                        // line 52
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["credential"], "consumerSecret", [])), "html", null, true);
                        echo "</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope=\"row\">Key Issued</th>
                                                                            <td>
                                                                                ";
                        // line 57
                        if ( !(null === $this->getAttribute($context["credential"], "issuedAt", []))) {
                            // line 58
                            echo "                                                                                    ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_date_format_filter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["credential"], "issuedAt", []), "date", [])), "M, m/d/Y - h:i"), "html", null, true);
                            echo "
                                                                                ";
                        } else {
                            // line 60
                            echo "                                                                                    ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["never"] ?? null)), "html", null, true);
                            echo "
                                                                                ";
                        }
                        // line 61
                        echo " 
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope=\"row\">Expires</th>
                                                                            <td>
                                                                                ";
                        // line 67
                        if ( !(null === $this->getAttribute($context["credential"], "expiresAt", []))) {
                            // line 68
                            echo "                                                                                    ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_date_format_filter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["credential"], "expiresAt", []), "date", [])), "M, m/d/Y - h:i"), "html", null, true);
                            echo "
                                                                                ";
                        } else {
                            // line 70
                            echo "                                                                                    ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["never"] ?? null)), "html", null, true);
                            echo "
                                                                                ";
                        }
                        // line 71
                        echo "    
                                                                            </td>
                                                                        </tr>                                                        
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            ";
                        // line 77
                        if ( !twig_test_empty($this->getAttribute($context["credential"], "apiProducts", []))) {
                            // line 78
                            echo "                                                                ";
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["credential"], "apiProducts", []));
                            foreach ($context['_seq'] as $context["_key"] => $context["apiProduct"]) {
                                // line 79
                                echo "                                                                    ";
                                $context["apiProductsMap"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["apiProductsMap"] ?? null)), [0 => [0 => $this->getAttribute($context["apiProduct"], "apiproduct", []), 1 => $this->getAttribute($context["apiProduct"], "status", [])]]);
                                // line 80
                                echo "                                                                    ";
                                $context["apiProducts"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["apiProducts"] ?? null)), [0 => $this->getAttribute($context["apiProduct"], "apiproduct", [])]);
                                // line 81
                                echo "                                                                ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['apiProduct'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 82
                            echo "                                                            ";
                        }
                        // line 83
                        echo "                                                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['credential'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 84
                    echo "                                                    ";
                }
                // line 85
                echo "                                                </div>
                                                <div class=\"tab-pane fade my-app-api-products\" id=\"my-app-api-products-";
                // line 86
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_replace_filter($this->sandbox->ensureToStringAllowed($this->getAttribute($context["developerApp"], "name", [])), "_", "-"), "html", null, true);
                echo "\">
                                                    ";
                // line 87
                if ( !twig_test_empty(($context["apiProductsMap"] ?? null))) {
                    // line 88
                    echo "                                                        <table class=\"table\">
                                                            <tbody>
                                                                ";
                    // line 90
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["apiProductsMap"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["value"]) {
                        // line 91
                        echo "                                                                    <tr>
                                                                        ";
                        // line 92
                        if ($this->getAttribute($context["value"], 0, [], "array", true, true)) {
                            // line 93
                            echo "                                                                            <td> <span>Status</span>";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["value"], 0, [], "array")), "html", null, true);
                            echo "</td>
                                                                        ";
                        }
                        // line 95
                        echo "                                                                        ";
                        if ($this->getAttribute($context["value"], 1, [], "array", true, true)) {
                            // line 96
                            echo "                                                                            <td class=\"last-right\">
                                                                                <span class=\"";
                            // line 97
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["value"], 1, [], "array")), "html", null, true);
                            echo "\">
                                                                                    ";
                            // line 98
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["value"], 1, [], "array")), "html", null, true);
                            echo "
                                                                                </span>
                                                                            </td>
                                                                        ";
                        }
                        // line 102
                        echo "                                                                    </tr>
                                                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 104
                    echo "                                                            </tbody>
                                                        </table>
                                                    ";
                }
                // line 107
                echo "                                                </div>
                                                <div class=\"tab-pane fade my-app-details\" id=\"my-app-details-";
                // line 108
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_replace_filter($this->sandbox->ensureToStringAllowed($this->getAttribute($context["developerApp"], "name", [])), "_", "-"), "html", null, true);
                echo "\">
                                                    <table class=\"table table-striped\">
                                                        <tbody>
                                                            <tr>
                                                                <th scope=\"row\">";
                // line 112
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["developerApp"], "displayName", [])), "html", null, true);
                echo "'s Details</th>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope=\"row\">Application Name</th>
                                                                <td>";
                // line 117
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["developerApp"], "displayName", [])), "html", null, true);
                echo "</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope=\"row\">API products</th>
                                                                <td>";
                // line 121
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_join_filter($this->sandbox->ensureToStringAllowed(($context["apiProducts"] ?? null)), ", "), "html", null, true);
                echo "</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope=\"row\">Description</th>
                                                                <td>";
                // line 125
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["developerApp"], "description", [])), "html", null, true);
                echo "</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope=\"row\">Callback URL</th>
                                                                <td>";
                // line 129
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["developerApp"], "callbackUrl", [])), "html", null, true);
                echo "</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope=\"row\">Status</th>
                                                                <td>
                                                                    <span class=\"";
                // line 134
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["developerApp"], "status", [])), "html", null, true);
                echo "\">
                                                                    ";
                // line 135
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["developerApp"], "status", [])), "html", null, true);
                echo "
                                                                    <span class=\"";
                // line 136
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["value"] ?? null), 1, [], "array")), "html", null, true);
                echo "\">
                                                                </td>
                                                            </tr> 
                                                        </tbody>
                                                    </table> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </dd>
                                ";
                // line 146
                $context["apiProducts"] = [];
                // line 147
                echo "                                ";
                $context["apiProductsMap"] = [];
                // line 148
                echo "                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['developerApp'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 149
            echo "                        </dl>
                    </div>
                ";
        }
        // line 152
        echo "            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "modules/custom/test_restapi/templates/open-banking-my-apps.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  401 => 152,  396 => 149,  390 => 148,  387 => 147,  385 => 146,  372 => 136,  368 => 135,  364 => 134,  356 => 129,  349 => 125,  342 => 121,  335 => 117,  327 => 112,  320 => 108,  317 => 107,  312 => 104,  305 => 102,  298 => 98,  294 => 97,  291 => 96,  288 => 95,  282 => 93,  280 => 92,  277 => 91,  273 => 90,  269 => 88,  267 => 87,  263 => 86,  260 => 85,  257 => 84,  251 => 83,  248 => 82,  242 => 81,  239 => 80,  236 => 79,  231 => 78,  229 => 77,  221 => 71,  215 => 70,  209 => 68,  207 => 67,  199 => 61,  193 => 60,  187 => 58,  185 => 57,  177 => 52,  170 => 48,  157 => 38,  154 => 37,  149 => 36,  147 => 35,  143 => 34,  132 => 29,  125 => 28,  118 => 27,  111 => 26,  104 => 25,  97 => 24,  88 => 18,  85 => 17,  81 => 16,  77 => 14,  75 => 13,  69 => 10,  61 => 4,  59 => 3,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% set apiProductsMap = {} %}
{% set apiProducts = {} %}
{% set never = 'Never' %}
<div class=\"my-apps-details\">
    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12\">
                <div class=\"my-app-add-button\">
                    <div class=\"btn btn-default\">
                        <a class=\"use-ajax\" data-dialog-type=\"modal\" href=\"{{ url('entity.developer_app.add_form_for_developer',{'user': user_id}) }}\"><label>+</label> Add an App</a>
                    </div>
                </div>               
                {% if my_apps is not empty%}
                    <div class=\"my-app-list-details\">
                        <dl class=\"ckeditor-accordion\">
                            {% for developerApp in my_apps %}
                                <dt>
                                    {{developerApp.displayName}}                                    
                                </dt>
                                <dd>
                                    <div class=\"panel with-nav-tabs panel-info\">
                                        <div class=\"panel-heading\">
                                            <ul class=\"nav nav-tabs\">
                                                <li class=\"active\"><a href=\"#my-app-keys-{{developerApp.name|replace('_','-')}}\" data-toggle=\"tab\"><img src=\"{{url('<front>')}}{{base_path ~ directory }}/images/key.png\">Keys</a></li>
                                                <li><a href=\"#my-app-api-products-{{developerApp.name|replace('_','-')}}\" data-toggle=\"tab\"><img src=\"{{url('<front>')}}{{base_path ~ directory }}/images/product.png\">Products</a></li>
                                                <li><a href=\"#my-app-details-{{developerApp.name|replace('_','-')}}\" data-toggle=\"tab\"><img src=\"{{url('<front>')}}{{base_path ~ directory }}/images/details.png\">Details</a></li>
                                                <li><a href=\"{{ url('test_restapi.my_app_analytics_controller_getMyAppName',{'app': developerApp.name}) }}\"><img src=\"{{url('<front>')}}{{base_path ~ directory }}/images/analytics.png\">Analytics</a></li>
                                                <li><a class=\"use-ajax\" data-dialog-type=\"modal\" href=\"{{ url('entity.developer_app.edit_form_for_developer',{'user': user_id,'app': developerApp.name}) }}\"><img src=\"{{url('<front>')}}{{base_path ~ directory }}/images/edit.png\">Edit</a></li>
                                                <li><a class=\"use-ajax\" data-dialog-type=\"modal\" href=\"{{ url('entity.developer_app.delete_form_for_developer',{'user': user_id,'app': developerApp.name}) }}\"><img src=\"{{url('<front>')}}{{base_path ~ directory }}/images/delete.png\">Delete</a></li>
                                            </ul>
                                        </div>
                                        <div class=\"panel-body\">
                                            <div class=\"tab-content\">
                                                <div class=\"tab-pane fade in active my-app-keys\" id=\"my-app-keys-{{developerApp.name|replace('_','-')}}\">
                                                    {%if developerApp.Credentials is not empty%}
                                                        {% for credential in developerApp.Credentials %}
                                                            <div class=\"my-apps-title\"
                                                                 <span>{{developerApp.displayName}}'s Keys</span>
                                                            </div>
                                                            <div class=\"my-apps-description\">
                                                                <p>Below are keys you can use to access the API products associated with this application (Delta digitalapi). The actual keys need to be approved and approved for an API product to be capable of accessing any of the URIs defined in the API product.</p>
                                                            </div>
                                                            <div class=\"my-apps-keys\">
                                                                <table class=\"table table-striped\">
                                                                    <tbody>
                                                                        <tr>
                                                                            <th scope=\"row\">Consumer Key</th>
                                                                            <td>{{credential.consumerKey}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope=\"row\">Consumer Secret</th>
                                                                            <td>{{credential.consumerSecret}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope=\"row\">Key Issued</th>
                                                                            <td>
                                                                                {% if credential.issuedAt is not null %}
                                                                                    {{credential.issuedAt.date|date('M, m/d/Y - h:i')}}
                                                                                {% else %}
                                                                                    {{never}}
                                                                                {% endif %} 
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope=\"row\">Expires</th>
                                                                            <td>
                                                                                {% if credential.expiresAt is not null %}
                                                                                    {{credential.expiresAt.date|date('M, m/d/Y - h:i')}}
                                                                                {% else %}
                                                                                    {{never}}
                                                                                {% endif %}    
                                                                            </td>
                                                                        </tr>                                                        
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            {% if credential.apiProducts is not empty%}
                                                                {% for apiProduct in credential.apiProducts %}
                                                                    {% set apiProductsMap = apiProductsMap|merge([[apiProduct.apiproduct,apiProduct.status]]) %}
                                                                    {% set apiProducts = apiProducts|merge([apiProduct.apiproduct]) %}
                                                                {% endfor %}
                                                            {% endif %}
                                                        {% endfor %}
                                                    {% endif %}
                                                </div>
                                                <div class=\"tab-pane fade my-app-api-products\" id=\"my-app-api-products-{{developerApp.name|replace('_','-')}}\">
                                                    {% if apiProductsMap is not empty %}
                                                        <table class=\"table\">
                                                            <tbody>
                                                                {% for value in apiProductsMap %}
                                                                    <tr>
                                                                        {% if value[0] is defined %}
                                                                            <td> <span>Status</span>{{value[0]}}</td>
                                                                        {% endif %}
                                                                        {% if value[1] is defined %}
                                                                            <td class=\"last-right\">
                                                                                <span class=\"{{value[1]}}\">
                                                                                    {{value[1]}}
                                                                                </span>
                                                                            </td>
                                                                        {% endif %}
                                                                    </tr>
                                                                {% endfor %}
                                                            </tbody>
                                                        </table>
                                                    {% endif %}
                                                </div>
                                                <div class=\"tab-pane fade my-app-details\" id=\"my-app-details-{{developerApp.name|replace('_','-')}}\">
                                                    <table class=\"table table-striped\">
                                                        <tbody>
                                                            <tr>
                                                                <th scope=\"row\">{{developerApp.displayName}}'s Details</th>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope=\"row\">Application Name</th>
                                                                <td>{{developerApp.displayName}}</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope=\"row\">API products</th>
                                                                <td>{{apiProducts|join(\", \")}}</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope=\"row\">Description</th>
                                                                <td>{{developerApp.description}}</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope=\"row\">Callback URL</th>
                                                                <td>{{developerApp.callbackUrl}}</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope=\"row\">Status</th>
                                                                <td>
                                                                    <span class=\"{{developerApp.status}}\">
                                                                    {{developerApp.status}}
                                                                    <span class=\"{{value[1]}}\">
                                                                </td>
                                                            </tr> 
                                                        </tbody>
                                                    </table> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </dd>
                                {% set apiProducts = {} %}
                                {% set apiProductsMap = {} %}
                            {% endfor %}
                        </dl>
                    </div>
                {% endif %}
            </div>
        </div>
    </div>
</div>", "modules/custom/test_restapi/templates/open-banking-my-apps.html.twig", "/Applications/MAMP/htdocs/oldob/ob-developer-portal/web/modules/custom/test_restapi/templates/open-banking-my-apps.html.twig");
    }
}
