<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/open_banking_portal_revised/templates/system/form/form--user-login-form.html.twig */
class __TwigTemplate_e0faf12b540eebc7f7b94c45c0fea62c708b7d9253087de970df9bbaa2266efb extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 4];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "
<div class=\"row login-form-popup\" id=\"login-form-popup\">
    <div class=\"login-right-form\">
        <form";
        // line 4
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null)), "html", null, true);
        echo ">
            ";
        // line 5
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["element"] ?? null), "status_messages", [])), "html", null, true);
        echo "
            ";
        // line 6
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["element"] ?? null), "name", [])), "html", null, true);
        echo "
            ";
        // line 7
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["element"] ?? null), "pass", [])), "html", null, true);
        echo "
            <div class=\"col-sm-6 remember-me\">
                <div id=\"example1\">
                ";
        // line 10
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["element"] ?? null), "persistent_login", [])), "html", null, true);
        echo "
                </div>
            </div>
            <div class=\"col-sm-6 forgot-pass-link\">
                <a class=\"pass-forgot\"  href='password?name'>Forgot password?</a>
            </div>
        
           ";
        // line 17
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["element"] ?? null), "actions", []), "submit", [])), "html", null, true);
        echo "
           ";
        // line 18
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["element"] ?? null), "form_build_id", [])), "html", null, true);
        echo " ";
        // line 19
        echo "            ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["element"] ?? null), "form_id", [])), "html", null, true);
        echo "
        </form>
            <p>Do not have an account? <a class=\"sign-up\"  href='register'> Sign Up Now</a></p>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "themes/custom/open_banking_portal_revised/templates/system/form/form--user-login-form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 19,  92 => 18,  88 => 17,  78 => 10,  72 => 7,  68 => 6,  64 => 5,  60 => 4,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("
<div class=\"row login-form-popup\" id=\"login-form-popup\">
    <div class=\"login-right-form\">
        <form{{attributes}}>
            {{element.status_messages}}
            {{element.name}}
            {{element.pass}}
            <div class=\"col-sm-6 remember-me\">
                <div id=\"example1\">
                {{element.persistent_login}}
                </div>
            </div>
            <div class=\"col-sm-6 forgot-pass-link\">
                <a class=\"pass-forgot\"  href='password?name'>Forgot password?</a>
            </div>
        
           {{element.actions.submit}}
           {{ element.form_build_id }} {# required #}
            {{ element.form_id }}
        </form>
            <p>Do not have an account? <a class=\"sign-up\"  href='register'> Sign Up Now</a></p>
    </div>
</div>
", "themes/custom/open_banking_portal_revised/templates/system/form/form--user-login-form.html.twig", "/Applications/MAMP/htdocs/oldob/ob-developer-portal/web/themes/custom/open_banking_portal_revised/templates/system/form/form--user-login-form.html.twig");
    }
}
