<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/contrib/apigee_edge/templates/status-property.html.twig */
class __TwigTemplate_3da5b7a6f5dc880ade43550513600b1ea4b5aad4e7a04de80ffe1081ea81b412 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 14];
        $filters = ["escape" => 19, "t" => 21, "capitalize" => 21, "render" => 21];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set'],
                ['escape', 't', 'capitalize', 'render'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 14
        $context["classes"] = [0 => "wrapper--status", 1 => $this->getAttribute($this->getAttribute(        // line 16
($context["element"] ?? null), "attributes", []), "class", [])];
        // line 19
        echo "<span ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
        echo ">
  <span class=\"label-status\">
    ";
        // line 21
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t(twig_capitalize_string_filter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed($this->getAttribute(($context["element"] ?? null), "value", []))))));
        echo "
  </span>
</span>
";
    }

    public function getTemplateName()
    {
        return "modules/contrib/apigee_edge/templates/status-property.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 21,  58 => 19,  56 => 16,  55 => 14,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Default theme implementation to status_property_element.
 *
 * Available variables:
 * - element: Element that will be rendered.
 * - element['value'] : the status string.
 *
 * @ingroup themeable
 */
#}
{%
  set classes = [
  'wrapper--status',
  element.attributes.class
  ]
%}
<span {{ attributes.addClass(classes) }}>
  <span class=\"label-status\">
    {{ element.value|render|capitalize|t }}
  </span>
</span>
", "modules/contrib/apigee_edge/templates/status-property.html.twig", "/Applications/MAMP/htdocs/oldob/ob-developer-portal/web/modules/contrib/apigee_edge/templates/status-property.html.twig");
    }
}
