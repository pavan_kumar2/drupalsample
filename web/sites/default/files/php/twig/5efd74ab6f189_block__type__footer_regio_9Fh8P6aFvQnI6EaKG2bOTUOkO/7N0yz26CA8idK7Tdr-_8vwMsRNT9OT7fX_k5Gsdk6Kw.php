<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/open_banking_portal_revised/templates/system/block/block__type__footer_region_block.html.twig */
class __TwigTemplate_8b70eab2427cec6740849befa2d076ca2294a0443ccd712c91b9f87e329f153a extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 13];
        $filters = ["escape" => 2];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "
<div ";
        // line 2
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "get-started-block experience-banking-api-block"], "method")), "html", null, true);
        echo "  >
    <div class=\"col-sm-6 get-started-left-image\">
        ";
        // line 4
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_banner_image", [])), "html", null, true);
        echo "
    </div>
    <div class=\"col-sm-6 get-started-right-body footer-part\">
        <div class=\"get-started-inner-part\">
            <h1>
                ";
        // line 9
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_block_title", [])), "html", null, true);
        echo "
            </h1>
            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 get-started-body right-banner-content-footer\">
                ";
        // line 12
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "body", [])), "html", null, true);
        echo "
                ";
        // line 13
        if ( !twig_test_empty($this->getAttribute(($context["content"] ?? null), "field_additional_buttons", []))) {
            // line 14
            echo "                    ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_additional_buttons", [])), "html", null, true);
            echo "
                ";
        }
        // line 16
        echo "            </div>
        </div>
    </div>

</div>


";
    }

    public function getTemplateName()
    {
        return "themes/custom/open_banking_portal_revised/templates/system/block/block__type__footer_region_block.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 16,  83 => 14,  81 => 13,  77 => 12,  71 => 9,  63 => 4,  58 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("
<div {{ attributes.addClass('get-started-block experience-banking-api-block') }}  >
    <div class=\"col-sm-6 get-started-left-image\">
        {{content.field_banner_image}}
    </div>
    <div class=\"col-sm-6 get-started-right-body footer-part\">
        <div class=\"get-started-inner-part\">
            <h1>
                {{content.field_block_title}}
            </h1>
            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 get-started-body right-banner-content-footer\">
                {{content.body}}
                {% if content.field_additional_buttons is not empty  %}
                    {{content.field_additional_buttons}}
                {% endif %}
            </div>
        </div>
    </div>

</div>


", "themes/custom/open_banking_portal_revised/templates/system/block/block__type__footer_region_block.html.twig", "/Applications/MAMP/htdocs/oldob/ob-developer-portal/web/themes/custom/open_banking_portal_revised/templates/system/block/block__type__footer_region_block.html.twig");
    }
}
