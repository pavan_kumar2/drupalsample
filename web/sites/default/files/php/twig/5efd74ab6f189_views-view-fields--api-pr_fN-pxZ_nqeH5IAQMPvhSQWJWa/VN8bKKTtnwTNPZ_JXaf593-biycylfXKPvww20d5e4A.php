<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/open_banking_portal_revised/templates/system/view/views-view-fields--api-products-homepage--block-1.html.twig */
class __TwigTemplate_0d7bf0b46fd8d09a1cd9a897f3356222c6797c12fa0b4e420153ebd9702e7a58 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 4];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "
    <div class=\"col-sm-12 col-xs-12 api-details\">
        <div class=\"first-half-body\">
        <h2>";
        // line 4
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["fields"] ?? null), "title", []), "content", [])), "html", null, true);
        echo "</h2>
        <hr class=\"line\"/>
        ";
        // line 6
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["fields"] ?? null), "body", []), "content", [])), "html", null, true);
        echo "
        </div>
        <div class=\"second-half-body\">
        <div class=\"col-sm-6 button-left-side\">
            ";
        // line 10
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["fields"] ?? null), "view_node", []), "content", [])), "html", null, true);
        echo "
        </div>
        <div class=\"col-sm-6 image-right-side\">
            ";
        // line 13
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["fields"] ?? null), "field_right_side_image", []), "content", [])), "html", null, true);
        echo "
        </div>
        </div>

    </div>



";
    }

    public function getTemplateName()
    {
        return "themes/custom/open_banking_portal_revised/templates/system/view/views-view-fields--api-products-homepage--block-1.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 13,  72 => 10,  65 => 6,  60 => 4,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("
    <div class=\"col-sm-12 col-xs-12 api-details\">
        <div class=\"first-half-body\">
        <h2>{{ fields.title.content }}</h2>
        <hr class=\"line\"/>
        {{ fields.body.content }}
        </div>
        <div class=\"second-half-body\">
        <div class=\"col-sm-6 button-left-side\">
            {{fields.view_node.content}}
        </div>
        <div class=\"col-sm-6 image-right-side\">
            {{fields.field_right_side_image.content}}
        </div>
        </div>

    </div>



", "themes/custom/open_banking_portal_revised/templates/system/view/views-view-fields--api-products-homepage--block-1.html.twig", "/Applications/MAMP/htdocs/oldob/ob-developer-portal/web/themes/custom/open_banking_portal_revised/templates/system/view/views-view-fields--api-products-homepage--block-1.html.twig");
    }
}
