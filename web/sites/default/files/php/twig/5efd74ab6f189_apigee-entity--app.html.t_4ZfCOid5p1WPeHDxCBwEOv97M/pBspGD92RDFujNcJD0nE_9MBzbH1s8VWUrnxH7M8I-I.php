<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/contrib/apigee_edge/templates/apigee-entity--app.html.twig */
class __TwigTemplate_bf135186d891e28d15661cdc58820a0fb1de61582350b1b64dd489366a4cadfb extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 43, "set" => 44, "include" => 50];
        $filters = ["clean_class" => 46];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'set', 'include'],
                ['clean_class'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 42
        echo "
";
        // line 43
        if (twig_in_filter($this->getAttribute(($context["entity"] ?? null), "getEntityTypeId", []), [0 => "developer_app", 1 => "team_app"])) {
            // line 44
            echo "  ";
            $context["classes"] = [0 => "apigee-entity--app", 1 => ("apigee-entity--app--view-mode-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(            // line 46
($context["view_mode"] ?? null))))];
        }
        // line 49
        echo "
";
        // line 50
        $this->loadTemplate("apigee-entity.html.twig", "modules/contrib/apigee_edge/templates/apigee-entity--app.html.twig", 50)->display($context);
    }

    public function getTemplateName()
    {
        return "modules/contrib/apigee_edge/templates/apigee-entity--app.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 50,  65 => 49,  62 => 46,  60 => 44,  58 => 43,  55 => 42,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Generic theme implementation to display a developer- or team app entity.

 * Available variables:
 * - entity: The entity with limited access to object properties and methods.
 *   Only method names starting with \"get\", \"has\", or \"is\" and a few common
 *   methods such as \"id\", \"label\", and \"bundle\" are available. For example:
 *   - entity.getEntityTypeId() will return the entity type ID.
 *   - entity.hasField('field_example') returns TRUE if the entity includes
 *     field_example. (This does not indicate the presence of a value in this
 *     field.)
 *   Calling other methods, such as entity.delete(), will result in an exception.
 *   See \\Drupal\\apigee_edge\\Entity\\EdgeEntityInterface for a full list of
 *   methods.
 * - label: The title of the entity.
 * - content: All rendered field items. Use {{ content }} to print them all,
 *   or print a subset such as {{ content.field_example }}. Use
 *   {{ content|without('field_example') }} to temporarily suppress the printing
 *   of a given child element.
 * - url: Direct URL of the current entity.
 * - attributes: HTML attributes for the containing element.
 * - title_attributes: Same as attributes, except applied to the main title
 *   tag that appears in the template.
 * - content_attributes: Same as attributes, except applied to the main
 *   content tag that appears in the template.
 * - title_prefix: Additional output populated by modules, intended to be
 *   displayed in front of the main title tag that appears in the template.
 * - title_suffix: Additional output populated by modules, intended to be
 *   displayed after the main title tag that appears in the template.
 * - view_mode: View mode; for example, \"teaser\" or \"full\".
 *
 * @see \\Drupal\\apigee_edge\\Entity\\EdgeEntityViewBuilder::getBuildDefaults()
 * @see \\Drupal\\Core\\Entity\\EntityViewBuilder::getBuildDefaults()
 * @see template_preprocess_apigee_entity()
 * @see https://www.drupal.org/project/drupal/issues/2808481
 *
 * @ingroup themeable
 */
#}

{% if entity.getEntityTypeId in ['developer_app', 'team_app'] %}
  {% set classes = [
    'apigee-entity--app',
    'apigee-entity--app--view-mode-' ~ view_mode|clean_class
  ] %}
{% endif %}

{% include 'apigee-entity.html.twig' %}
", "modules/contrib/apigee_edge/templates/apigee-entity--app.html.twig", "/Applications/MAMP/htdocs/oldob/ob-developer-portal/web/modules/contrib/apigee_edge/templates/apigee-entity--app.html.twig");
    }
}
