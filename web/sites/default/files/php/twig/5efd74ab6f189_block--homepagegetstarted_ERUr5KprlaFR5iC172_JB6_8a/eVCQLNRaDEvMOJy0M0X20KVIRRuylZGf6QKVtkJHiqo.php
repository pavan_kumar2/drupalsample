<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/open_banking_portal_revised/templates/system/block/block--homepagegetstartedblock.html.twig */
class __TwigTemplate_f87751bd6b41eb0786b9c3c85d619fa4de85f30b0250d0c5c7ddd47420050aeb extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 1];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "get-started-block"], "method")), "html", null, true);
        echo " >
    <div class=\"col-sm-6 get-started-left-image\">
      ";
        // line 3
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_image", [])), "html", null, true);
        echo "
    </div>
    <div class=\"col-sm-6 get-started-right-body\">
      <div class=\"get-started-inner-part\">
        ";
        // line 7
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_block_title", [])), "html", null, true);
        echo "
        <h2>";
        // line 8
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_block_sub_title", [])), "html", null, true);
        echo "</h2><hr class=\"get-started-line\">
        <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 get-started-body\">
          ";
        // line 10
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "body", [])), "html", null, true);
        echo "
        </div>
        <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 get-started-buttons\">
          ";
        // line 13
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_addon_buttons", [])), "html", null, true);
        echo "
        </div>
      </div>
    </div>

  </div>


";
    }

    public function getTemplateName()
    {
        return "themes/custom/open_banking_portal_revised/templates/system/block/block--homepagegetstartedblock.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 13,  77 => 10,  72 => 8,  68 => 7,  61 => 3,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<div {{ attributes.addClass('get-started-block') }} >
    <div class=\"col-sm-6 get-started-left-image\">
      {{content.field_image}}
    </div>
    <div class=\"col-sm-6 get-started-right-body\">
      <div class=\"get-started-inner-part\">
        {{content.field_block_title}}
        <h2>{{content.field_block_sub_title}}</h2><hr class=\"get-started-line\">
        <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 get-started-body\">
          {{content.body}}
        </div>
        <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 get-started-buttons\">
          {{content.field_addon_buttons}}
        </div>
      </div>
    </div>

  </div>


", "themes/custom/open_banking_portal_revised/templates/system/block/block--homepagegetstartedblock.html.twig", "/Applications/MAMP/htdocs/oldob/ob-developer-portal/web/themes/custom/open_banking_portal_revised/templates/system/block/block--homepagegetstartedblock.html.twig");
    }
}
