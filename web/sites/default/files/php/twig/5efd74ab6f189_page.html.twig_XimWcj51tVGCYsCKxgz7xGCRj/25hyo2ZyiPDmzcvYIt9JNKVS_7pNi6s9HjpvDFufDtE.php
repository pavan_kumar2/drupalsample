<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/open_banking_portal_revised/templates/system/page.html.twig */
class __TwigTemplate_8574f1df2cdf786f78c5380d5d2bbcd7740b1c13b9d4be3f207094d703f14434 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'navbar' => [$this, 'block_navbar'],
            'content_header' => [$this, 'block_content_header'],
            'main' => [$this, 'block_main'],
            'header' => [$this, 'block_header'],
            'sidebar_first' => [$this, 'block_sidebar_first'],
            'highlighted' => [$this, 'block_highlighted'],
            'help' => [$this, 'block_help'],
            'content' => [$this, 'block_content'],
            'sidebar_second' => [$this, 'block_sidebar_second'],
            'content_footer' => [$this, 'block_content_footer'],
            'footer' => [$this, 'block_footer'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 55, "if" => 57, "block" => 58];
        $filters = ["escape" => 54, "clean_class" => 62, "t" => 75];
        $functions = ["attach_library" => 54];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'block'],
                ['escape', 'clean_class', 't'],
                ['attach_library']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 54
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("core/drupal.dialog.ajax"), "html", null, true);
        echo "
";
        // line 55
        $context["container"] = (($this->getAttribute($this->getAttribute(($context["theme"] ?? null), "settings", []), "fluid_container", [])) ? ("container-fluid") : ("container"));
        // line 57
        if (($this->getAttribute(($context["page"] ?? null), "navigation", []) || $this->getAttribute(($context["page"] ?? null), "navigation_collapsible", []))) {
            // line 58
            echo "  ";
            $this->displayBlock('navbar', $context, $blocks);
        }
        // line 106
        echo "
";
        // line 108
        $this->displayBlock('main', $context, $blocks);
        // line 176
        echo "<section class=\"open_banking_content_footer\">
  <div class=\"";
        // line 177
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo "\">
    ";
        // line 179
        echo "    ";
        if ($this->getAttribute(($context["page"] ?? null), "content_footer", [])) {
            // line 180
            echo "      ";
            $this->displayBlock('content_footer', $context, $blocks);
            // line 185
            echo "    ";
        }
        // line 186
        echo "  </div>
</section>
<section class=\"open_banking_footer\">
  <div class=\"";
        // line 189
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo "\">
    ";
        // line 190
        if ($this->getAttribute(($context["page"] ?? null), "footer", [])) {
            // line 191
            echo "      ";
            $this->displayBlock('footer', $context, $blocks);
            // line 196
            echo "    ";
        }
        // line 197
        echo "  </div>
</section>

";
    }

    // line 58
    public function block_navbar($context, array $blocks = [])
    {
        // line 59
        echo "    ";
        // line 60
        $context["navbar_classes"] = [0 => "navbar", 1 => (($this->getAttribute($this->getAttribute(        // line 62
($context["theme"] ?? null), "settings", []), "navbar_position", [])) ? (("navbar-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["theme"] ?? null), "settings", []), "navbar_position", []))))) : (($context["container"] ?? null)))];
        // line 65
        echo "    <section class=\"open_banking_header\">
      <header";
        // line 66
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["navbar_attributes"] ?? null), "addClass", [0 => ($context["navbar_classes"] ?? null)], "method")), "html", null, true);
        echo " id=\"navbar\" role=\"banner\">
        ";
        // line 67
        if ( !$this->getAttribute(($context["navbar_attributes"] ?? null), "hasClass", [0 => ($context["container"] ?? null)], "method")) {
            // line 68
            echo "        <div class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
            echo "\">
          ";
        }
        // line 70
        echo "          <div class=\"navbar-header\">
            ";
        // line 71
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navigation", [])), "html", null, true);
        echo "
            ";
        // line 73
        echo "            ";
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])) {
            // line 74
            echo "              <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#navbar-collapse\">
                <span class=\"sr-only\">";
            // line 75
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Toggle navigation"));
            echo "</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
              </button>
            ";
        }
        // line 81
        echo "          </div>

          ";
        // line 84
        echo "          ";
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])) {
            // line 85
            echo "            <div id=\"navbar-collapse\" class=\"navbar-collapse collapse\">
              ";
            // line 86
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])), "html", null, true);
            echo "
            </div>
          ";
        }
        // line 89
        echo "          ";
        if ( !$this->getAttribute(($context["navbar_attributes"] ?? null), "hasClass", [0 => ($context["container"] ?? null)], "method")) {
            // line 90
            echo "        </div>
        ";
        }
        // line 92
        echo "      </header>
      <div class=\"";
        // line 93
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo " main-container\">
        ";
        // line 95
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "content_header", [])) {
            // line 96
            echo "          ";
            $this->displayBlock('content_header', $context, $blocks);
            // line 101
            echo "        ";
        }
        // line 102
        echo "      </div>
    </section>
  ";
    }

    // line 96
    public function block_content_header($context, array $blocks = [])
    {
        // line 97
        echo "            <div class=\"col-sm-12 content_header\" role=\"content_header\">
              ";
        // line 98
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_header", [])), "html", null, true);
        echo "
            </div>
          ";
    }

    // line 108
    public function block_main($context, array $blocks = [])
    {
        // line 109
        echo "  <div role=\"main\" class=\"main-container js-quickedit-main-content\">
    <div class=\"row\">

      ";
        // line 113
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "header", [])) {
            // line 114
            echo "        ";
            $this->displayBlock('header', $context, $blocks);
            // line 119
            echo "      ";
        }
        // line 120
        echo "
      ";
        // line 122
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])) {
            // line 123
            echo "        ";
            $this->displayBlock('sidebar_first', $context, $blocks);
            // line 128
            echo "      ";
        }
        // line 129
        echo "      ";
        // line 130
        echo "      ";
        // line 131
        $context["content_classes"] = [0 => ((($this->getAttribute(        // line 132
($context["page"] ?? null), "sidebar_first", []) && $this->getAttribute(($context["page"] ?? null), "sidebar_second", []))) ? ("col-sm-6") : ("")), 1 => ((($this->getAttribute(        // line 133
($context["page"] ?? null), "sidebar_first", []) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])))) ? ("col-sm-9") : ("")), 2 => ((($this->getAttribute(        // line 134
($context["page"] ?? null), "sidebar_second", []) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])))) ? ("col-sm-9") : ("")), 3 => (((twig_test_empty($this->getAttribute(        // line 135
($context["page"] ?? null), "sidebar_first", [])) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])))) ? ("col-sm-12") : ("")), 4 => "padding-reset"];
        // line 140
        echo "      <section";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content_attributes"] ?? null), "addClass", [0 => ($context["content_classes"] ?? null)], "method")), "html", null, true);
        echo ">



        ";
        // line 145
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", [])) {
            // line 146
            echo "          ";
            $this->displayBlock('highlighted', $context, $blocks);
            // line 149
            echo "        ";
        }
        // line 150
        echo "
        ";
        // line 152
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "help", [])) {
            // line 153
            echo "          ";
            $this->displayBlock('help', $context, $blocks);
            // line 156
            echo "        ";
        }
        // line 157
        echo "
        ";
        // line 159
        echo "        ";
        $this->displayBlock('content', $context, $blocks);
        // line 163
        echo "      </section>

      ";
        // line 166
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])) {
            // line 167
            echo "        ";
            $this->displayBlock('sidebar_second', $context, $blocks);
            // line 172
            echo "      ";
        }
        // line 173
        echo "    </div>
  </div>
";
    }

    // line 114
    public function block_header($context, array $blocks = [])
    {
        // line 115
        echo "          <div class=\"col-sm-12\" role=\"heading\">
            ";
        // line 116
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
        echo "
          </div>
        ";
    }

    // line 123
    public function block_sidebar_first($context, array $blocks = [])
    {
        // line 124
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 125
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])), "html", null, true);
        echo "
          </aside>
        ";
    }

    // line 146
    public function block_highlighted($context, array $blocks = [])
    {
        // line 147
        echo "            <div class=\"highlighted\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "highlighted", [])), "html", null, true);
        echo "</div>
          ";
    }

    // line 153
    public function block_help($context, array $blocks = [])
    {
        // line 154
        echo "            ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "help", [])), "html", null, true);
        echo "
          ";
    }

    // line 159
    public function block_content($context, array $blocks = [])
    {
        // line 160
        echo "          <a id=\"main-content\"></a>
          ";
        // line 161
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "
        ";
    }

    // line 167
    public function block_sidebar_second($context, array $blocks = [])
    {
        // line 168
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 169
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])), "html", null, true);
        echo "
          </aside>
        ";
    }

    // line 180
    public function block_content_footer($context, array $blocks = [])
    {
        // line 181
        echo "        <div class=\"col-sm-12 content_footer\" role=\"content_footer\">
          ";
        // line 182
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_footer", [])), "html", null, true);
        echo "
        </div>
      ";
    }

    // line 191
    public function block_footer($context, array $blocks = [])
    {
        // line 192
        echo "        <footer class=\"footer\" role=\"contentinfo\">
          ";
        // line 193
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer", [])), "html", null, true);
        echo "
        </footer>
      ";
    }

    public function getTemplateName()
    {
        return "themes/custom/open_banking_portal_revised/templates/system/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  409 => 193,  406 => 192,  403 => 191,  396 => 182,  393 => 181,  390 => 180,  383 => 169,  380 => 168,  377 => 167,  371 => 161,  368 => 160,  365 => 159,  358 => 154,  355 => 153,  348 => 147,  345 => 146,  338 => 125,  335 => 124,  332 => 123,  325 => 116,  322 => 115,  319 => 114,  313 => 173,  310 => 172,  307 => 167,  304 => 166,  300 => 163,  297 => 159,  294 => 157,  291 => 156,  288 => 153,  285 => 152,  282 => 150,  279 => 149,  276 => 146,  273 => 145,  265 => 140,  263 => 135,  262 => 134,  261 => 133,  260 => 132,  259 => 131,  257 => 130,  255 => 129,  252 => 128,  249 => 123,  246 => 122,  243 => 120,  240 => 119,  237 => 114,  234 => 113,  229 => 109,  226 => 108,  219 => 98,  216 => 97,  213 => 96,  207 => 102,  204 => 101,  201 => 96,  198 => 95,  194 => 93,  191 => 92,  187 => 90,  184 => 89,  178 => 86,  175 => 85,  172 => 84,  168 => 81,  159 => 75,  156 => 74,  153 => 73,  149 => 71,  146 => 70,  140 => 68,  138 => 67,  134 => 66,  131 => 65,  129 => 62,  128 => 60,  126 => 59,  123 => 58,  116 => 197,  113 => 196,  110 => 191,  108 => 190,  104 => 189,  99 => 186,  96 => 185,  93 => 180,  90 => 179,  86 => 177,  83 => 176,  81 => 108,  78 => 106,  74 => 58,  72 => 57,  70 => 55,  66 => 54,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Default theme implementation to display a single page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.html.twig template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - base_path: The base URL path of the Drupal installation. Will usually be
 *   \"/\" unless you have installed Drupal in a sub-directory.
 * - is_front: A flag indicating if the current page is the front page.
 * - logged_in: A flag indicating if the user is registered and signed in.
 * - is_admin: A flag indicating if the user has permission to access
 *   administration pages.
 *
 * Site identity:
 * - front_page: The URL of the front page. Use this instead of base_path when
 *   linking to the front page. This includes the language domain or prefix.
 *
 * Page content (in order of occurrence in the default page.html.twig):
 * - title_prefix: Additional output populated by modules, intended to be
 *   displayed in front of the main title tag that appears in the template.
 * - title: The page title, for use in the actual content.
 * - title_suffix: Additional output populated by modules, intended to be
 *   displayed after the main title tag that appears in the template.
 * - messages: Status and error messages. Should be displayed prominently.
 * - tabs: Tabs linking to any sub-pages beneath the current page (e.g., the
 *   view and edit tabs when displaying a node).
 * - node: Fully loaded node, if there is an automatically-loaded node
 *   associated with the page and the node ID is the second argument in the
 *   page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - page.header: Items for the header region.
 * - page.navigation: Items for the navigation region.
 * - page.navigation_collapsible: Items for the navigation (collapsible) region.
 * - page.highlighted: Items for the highlighted content region.
 * - page.help: Dynamic help text, mostly for admin pages.
 * - page.content: The main content of the current page.
 * - page.sidebar_first: Items for the first sidebar.
 * - page.sidebar_second: Items for the second sidebar.
 * - page.footer: Items for the footer region.
 *
 * @ingroup templates
 *
 * @see template_preprocess_page()
 * @see html.html.twig
 */
#}
{{ attach_library('core/drupal.dialog.ajax') }}
{% set container = theme.settings.fluid_container ? 'container-fluid' : 'container' %}
{# Navbar #}
{% if page.navigation or page.navigation_collapsible %}
  {% block navbar %}
    {%
      set navbar_classes = [
      'navbar',
      theme.settings.navbar_position ? 'navbar-' ~ theme.settings.navbar_position|clean_class : container,
    ]
    %}
    <section class=\"open_banking_header\">
      <header{{ navbar_attributes.addClass(navbar_classes) }} id=\"navbar\" role=\"banner\">
        {% if not navbar_attributes.hasClass(container) %}
        <div class=\"{{ container }}\">
          {% endif %}
          <div class=\"navbar-header\">
            {{ page.navigation }}
            {# .btn-navbar is used as the toggle for collapsed navbar content #}
            {% if page.navigation_collapsible %}
              <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#navbar-collapse\">
                <span class=\"sr-only\">{{ 'Toggle navigation'|t }}</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
              </button>
            {% endif %}
          </div>

          {# Navigation (collapsible) #}
          {% if page.navigation_collapsible %}
            <div id=\"navbar-collapse\" class=\"navbar-collapse collapse\">
              {{ page.navigation_collapsible }}
            </div>
          {% endif %}
          {% if not navbar_attributes.hasClass(container) %}
        </div>
        {% endif %}
      </header>
      <div class=\"{{container}} main-container\">
        {# Content Header #}
        {% if page.content_header %}
          {% block content_header %}
            <div class=\"col-sm-12 content_header\" role=\"content_header\">
              {{ page.content_header }}
            </div>
          {% endblock %}
        {% endif %}
      </div>
    </section>
  {% endblock %}
{% endif %}

{# Main #}
{% block main %}
  <div role=\"main\" class=\"main-container js-quickedit-main-content\">
    <div class=\"row\">

      {# Header #}
      {% if page.header %}
        {% block header %}
          <div class=\"col-sm-12\" role=\"heading\">
            {{ page.header }}
          </div>
        {% endblock %}
      {% endif %}

      {# Sidebar First #}
      {% if page.sidebar_first %}
        {% block sidebar_first %}
          <aside class=\"col-sm-3\" role=\"complementary\">
            {{ page.sidebar_first }}
          </aside>
        {% endblock %}
      {% endif %}
      {# Content #}
      {%
        set content_classes = [
        page.sidebar_first and page.sidebar_second ? 'col-sm-6',
        page.sidebar_first and page.sidebar_second is empty ? 'col-sm-9',
        page.sidebar_second and page.sidebar_first is empty ? 'col-sm-9',
        page.sidebar_first is empty and page.sidebar_second is empty ? 'col-sm-12',
        'padding-reset',
      ]

      %}
      <section{{ content_attributes.addClass(content_classes) }}>



        {# Highlighted #}
        {% if page.highlighted %}
          {% block highlighted %}
            <div class=\"highlighted\">{{ page.highlighted }}</div>
          {% endblock %}
        {% endif %}

        {# Help #}
        {% if page.help %}
          {% block help %}
            {{ page.help }}
          {% endblock %}
        {% endif %}

        {# Content #}
        {% block content %}
          <a id=\"main-content\"></a>
          {{ page.content }}
        {% endblock %}
      </section>

      {# Sidebar Second #}
      {% if page.sidebar_second %}
        {% block sidebar_second %}
          <aside class=\"col-sm-3\" role=\"complementary\">
            {{ page.sidebar_second }}
          </aside>
        {% endblock %}
      {% endif %}
    </div>
  </div>
{% endblock %}
<section class=\"open_banking_content_footer\">
  <div class=\"{{ container }}\">
    {# Content Footer #}
    {% if page.content_footer %}
      {% block content_footer %}
        <div class=\"col-sm-12 content_footer\" role=\"content_footer\">
          {{ page.content_footer }}
        </div>
      {% endblock %}
    {% endif %}
  </div>
</section>
<section class=\"open_banking_footer\">
  <div class=\"{{container}}\">
    {% if page.footer %}
      {% block footer %}
        <footer class=\"footer\" role=\"contentinfo\">
          {{ page.footer }}
        </footer>
      {% endblock %}
    {% endif %}
  </div>
</section>

", "themes/custom/open_banking_portal_revised/templates/system/page.html.twig", "/Applications/MAMP/htdocs/oldob/ob-developer-portal/web/themes/custom/open_banking_portal_revised/templates/system/page.html.twig");
    }
}
